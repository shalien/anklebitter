import discordjs from 'discord.js';
import fs from 'fs';
import { EOL } from 'os';
const {Client, Attachment, DMChannel } = discordjs;
const client = new Client();

const GALERIE_GUILD = process.env.TARGET;


export default class DiscordBot {


    constructor() {

        this.guildsKnow = [];

        this.client = new Client();

        this.client.on('ready', () => {
            console.log('Client is ready');
        });
        
        this.client.on('guildCreate', async (guild) => {
            if (guild.id === GALERIE_GUILD) return;
        
            console.log(guild.name + ' ' + guild.id);
        
            console.log("Joined " + guild.name);
        
            const sub = await getGuildSub(guild);
        
            await createCategoryIfNotExists(client, sub);
        
            const { channels, } = guild;
            for(const subChannel of channels.array()) {
                if (subChannel.type === "text"
                    && subChannel.permissionsFor(client.user).has("READ_MESSAGES")
                    && subChannel.permissionsFor(client.user).has("READ_MESSAGE_HISTORY")) {
        
                    const collection = await subChannel.fetchMessages();
                    const withAttachment = await collection.filter(message => message.attachments !== null);
        
                    for (const message of withAttachment.array()) {
                        for(const attachment of message.attachments.array()) {
        
                            const link = attachment.url;
        
                            if (checkExtension(link) && !alreadySeenMessage(sub, message.id)) {
                                const localNewCategory = await getCategoryChannel(client, sub);
                                await createChannelInCategoryIfNotExists(client, localNewCategory.name, subChannel.name);
        
                                const localChannel = getChildrenChannel(localNewCategory, subChannel.name);
        
                                const reattach = new Attachment(link);
                      
                                try {
                                    await localChannel.send(reattach);
                                    } catch (error) {
                                        console.log(error);
                                    } finally {
                                        fs.writeFileSync('guilds/' + sub + '.txt', message.id + EOL);
                                        guildsKnow[sub].push(message.id);
                                    }
                            }
                        }
                    }
                }
            }
        });
        
        this.client.on('message', async (onMessage) => {
        
            if(onMessage.channel instanceof DMChannel) {
                if (onMessage.author.id == '790324759803461702') {
                    console.log('message from CeGars');
                    const words = onMessage.content.toLocaleLowerCase().trim().split(' ');
            
                    if(words[0] == 'guild') {
                        const guildId = words[1];
            
                        const guild = client.guilds.find(g => guildId == g.id);
            
                        if(guild == null) return;
            
                        console.log("Scanning " + guild.name);
            
                        const sub = await getGuildSub(guild);
            
                        await createCategoryIfNotExists(client, sub);
                    
                        const { channels, } = guild;
                        for(const subChannel of channels.array()) {
                            if (subChannel.type === "text"
                                && subChannel.permissionsFor(client.user).has("READ_MESSAGES")
                                && subChannel.permissionsFor(client.user).has("READ_MESSAGE_HISTORY")) {
                    
                                const collection = await subChannel.fetchMessages();
                                const withAttachment = await collection.filter(message => message.attachments !== null);
                    
                                for (const message of withAttachment.array()) {
                                    console.log(message.id);
                                    if(message == undefined) continue;
                                    for(const attachment of message.attachments.array()) {
                    
                                        const link = attachment.url;
                    
                                        if (checkExtension(link) && !alreadySeenMessage(sub, message.id)) {
                                            const localNewCategory = await getCategoryChannel(client, sub);
                                            await createChannelInCategoryIfNotExists(client, localNewCategory.name, subChannel.name);
                    
                                            const localChannel = getChildrenChannel(localNewCategory, subChannel.name);
                    
                                            const reattach = new Attachment(link);
                                  
                                            try {
                                                await localChannel.send(reattach);
                                                } catch (error) {
                                                    console.log(error);
                                                } finally {
                                                    if(message == null) return;
                                                    fs.writeFileSync('guilds/' + sub + '.txt', message.id + EOL);
                                                    guildsKnow[sub].push(message.id);
                                                }
                                        }
                                    }
                                }
                            }
                        }
            
                    }
            
                }
                return;
            }
        
            if (onMessage.guild == null || onMessage.guild.id === GALERIE_GUILD) return;
            if (onMessage.attachments !== null) {
        
                for(const attachment of onMessage.attachments.array()) {
                    const link = attachment.url;
        
                    const sub = await getGuildSub(onMessage.guild);
        
                    if (checkExtension(link) && !alreadySeenMessage(sub, onMessage.id)) {
                        await createCategoryIfNotExists(client, sub);
        
                        const localNewCategory = await getCategoryChannel(client, sub);
                        await createChannelInCategoryIfNotExists(client, localNewCategory.name, onMessage.channel.name);
        
                        const localChannel = getChildrenChannel(localNewCategory, onMessage.channel.name);
        
                        const reattach = new Attachment(link);
        
                        console.log("Uploading " + reattach + " in " + localChannel.name);
        
                        try {
                        await localChannel.send(reattach);
                        } catch (error) {
                            console.log(error);
                        } finally {
                            fs.writeFileSync('guilds/' + sub + '.txt', onMessage.id + EOL);
                            guildsKnow[sub].push(onMessage.id);
                        }
                    }
                }
        
        
            }
        
        });
        
        
        
        if(!fs.existsSync('guilds')) fs.mkdirSync('guilds');
        
        for(const file of fs.readdirSync('guilds')) {
        
            const lines = fs.readFileSync('guilds/' + file).toString().split('EOL');
        
            guildsKnow[file.split('/').pop()] = lines;
        }
        
        this.client.login(process.env.TOKEN);
    }

    getGalerie(client) {
        return client.guilds.find(theguild => theguild.id === GALERIE_GUILD);
    }
    
    getChildrenChannel(category, child) {
        return category.children.find(categoryChild => categoryChild.name === child);
    }
    
    getCategoryChannel(client, category) {
       const galerie =  getGalerie(client);
    return galerie.channels.find(channel => channel.name === category);
    }
    
    getGuildSub(guild) {
        return guild.id.substring(0, 4);
    }
    
    sleep(millis) {
        return new Promise(resolve => setTimeout(resolve, millis));
    }
    
    async createCategoryIfNotExists(client, categoryName) {
    
        const galerie = getGalerie(client);
    
        console.log(await getCategoryChannel(client, categoryName));
    
        if (await getCategoryChannel(client, categoryName) === null) {
            await galerie.createChannel(categoryName, {type: "category"});
        } else {
            console.log('Category ' + categoryName + " exists !");
        }
    
    
    }
    
    async createChannelInCategoryIfNotExists(client, categoryName, channelName) {
    
        const galerie = getGalerie(client);
    
        const category = await getCategoryChannel(client, categoryName);
    
        if (!category) return;
    
        if (await getChildrenChannel(category, channelName) === null) {
            const newChannel = await galerie.createChannel(channelName, {type: 'text'});
            await newChannel.setParent(category);
        } else {
            console.log("Channel " + channelName + " already exsits");
        }
    
    }
    
    alreadySeenMessage(sub, messageId) {
        if(guildsKnow[sub] === undefined) {
            guildsKnow[sub] = [];
            return false;
        } else {
          return guildsKnow[sub].find(element => element == messageId)
        }
    }
    
    checkExtension(filename) {
        const ext = filename.toLowerCase().trim().split('.').pop();
    
        switch (ext) {
            case 'png':
            case 'jpg':
            case 'jpeg':
            case 'gif':
            case 'jiff':
            case 'mov':
            case 'mp4':
            case 'webm':
            case 'webp':
                return true;
            default:
                return false;
        }
    }

}